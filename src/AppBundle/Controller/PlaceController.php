<?php

namespace AppBundle\Controller;
use AppBundle\Form\PlaceType;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Place;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;



class PlaceController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/places")
     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     */
    public function getPlacesAction(Request $request, ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        $qb = $this->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $qb->select('p')->from('AppBundle:Place', 'p');
        if ($offset != ""){
            $qb->setFirstResult($offset);
        }

        if($limit != ""){
            $qb->setMaxResults($limit);
        }

        if (in_array($sort, ['asc', 'desc'])){
            $qb->orderBy('p.name', $sort);
        }

        $places = $qb->getQuery()->getResult();

        return $places;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/places/{id}")
     */

    public function getPlaceAction(Request $request)
    {
        $place = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Place')
            ->find($request->get('id'));

        /* @var $place Place */

        if (empty($place)){
            return $this->placeNotFound();
        }

        return $place;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"place"})
     * @Rest\Post("/places")
     */

    public function postPlacesAction(Request $request)
    {
        $place = new Place();
        $form = $this->createForm(PlaceType::class, $place);

        $form->submit($request->request->all());// Validation des données

        if ($form->isValid()){

            $em = $this->get('doctrine.orm.entity_manager');
            foreach ($place->getPrices() as $price){
                $price->setPlace($place);
                $em->persist($price);
            }
            $em->persist($place);
            $em->flush();

            return $place;
        }
        else{
            return $form;
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/places/{id}")
     */
    public function removePlaceAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $place = $em->getRepository('AppBundle:Place')
                    ->find($request->get('id'));
        /* @var $place \AppBundle\Entity\Place*/

        if(!$place){
            return ;
        }
        // supprimer les prix associer aussi
        foreach ($place->getPrices() as $price){
            $em->remove($price);
        }

        $em->remove($place);
        $em->flush();
    }

    /**
    * @Rest\View()
    * @Rest\Put("/places/{id}")
    */
    public function updatePlaceAction(Request $request)
    {
        return $this->updatePlace($request, true);
    }

    /**
     * @Rest\View()
     * @Rest\Patch("/places/{id}")
     */
    public function patchPlaceAction(Request $request)
    {
        return $this->updatePlace($request, false);
    }


    private function updatePlace(Request $request, $clearMissing)
    {
        $place = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Place')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $place Place */

        if (empty($place)) {
            return new JsonResponse(['message' => 'Cette place n\'existe pas '], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(PlaceType::class, $place);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($place);
            $em->flush();
            return $place;
        } else {
            return $form;
        }
    }

    public function placeNotFound ()
    {
        return \FOS\RestBundle\View\View::create(['message'=>'Place not Found'], Response::HTTP_NOT_FOUND);
    }
}