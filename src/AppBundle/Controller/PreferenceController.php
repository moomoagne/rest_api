<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Preference;
use AppBundle\Form\PreferenceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;


class PreferenceController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"preference"})
     * @Rest\Get("/users/{id}/preferences")
     */
    public function getPreferencesAction (Request $request)
    {
        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->find($request->get('id'));
        /* @var $user User */

        if(empty($user)){
            return new JsonResponse(['message' => 'Utilisateur non trouver'], Response::HTTP_NOT_FOUND);
        }
        //dump($user->getPreferences());die();
        return $user->getPreferences();
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"preference"})
     * @Rest\Post("/users/{id}/preferences")
     */
    public function postPreferencesAction (Request $request)
    {
        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->find($request->get('id'));
        /* @var $user User */

        if(empty($user)){
            return new JsonResponse(['message' => 'Utilisateur no trouver'], Response::HTTP_NOT_FOUND);
        }

        $preference = new Preference();
        $user->setPreferences($preference);

        $form = $this->createForm(PreferenceType::class, $preference);
        $form->submit($request->request->all());
        if($form->isValid()){
            $em =  $this->get('doctrine.orm.entity_manager');
            $em->persist($preference);
            $em->flush();
        }else{
            return $form;
        }

    }
}
