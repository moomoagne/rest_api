<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\PriceType;

class PlaceType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('prices', CollectionType::class, [
                'entry_type' => PriceType::class,
                'allow_add' => true,
                'error_bubbling' => false,
            ]);
    }
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => 'AppBundle\Entity\Place',
            'csrf_protection' => false
        ]);
    }

    /**
     {@inheritdoc}

    public function getBlockPrefix()
    {
        return 'appbundle_place';
    }*/

}
