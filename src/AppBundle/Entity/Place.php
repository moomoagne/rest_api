<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
//use JMS\Serializer\Annotation as JMS ;

/**
 * Place
 *
 * @ORM\Table(name="places",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="places_name_unique", columns={"name"})}
 *     )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlaceRepository")
 *
 *
 */
//@JMS\ExclusionPolicy("all")
class Place
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    //@JMS\Groups({"place"})
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="Price", mappedBy="place")
     * @var Price[]
     */
    protected $prices;


    /**
    * @ORM\OneToMany(targetEntity="Theme", mappedBy="place")
    * @var Theme[]
    */
    protected $themes;

    /**
     * @return Theme[]
     */
    public function getThemes ()
    {
        return $this->themes;
    }

    /**
     * @param Theme[] $themes
     */
    public function setThemes ($themes)
    {
        $this->themes = $themes;
    }

    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param Price[] $prices
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Place
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Place
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->themes = new ArrayCollection();
    }
}

